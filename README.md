# Spark EOS logs parser

This project processes daily folders of EOS logs and creates the equivalent in Parquet format.

## Build command

The command used to build the project

```bash
git clone 
cd spark-EOS-logs-parser
mvn clean package -DskipTests
```

Artifacts can be found in the ``./target/`` folder

## Run command

Assuming data are stored in a Hadoop cluster and that the application will run in cluster mode:

```bash
EOS_PROJECT=cms
MAX_THREADS_NUMBER=50
MAX_EXECUTORS=50
EXECUTOR_MEMORY=5G
EXECUTOR_CORES=1
ENTRIES_PER_FILE=10000000
CODEC=snappy #lz4 preferred for alice
spark-submit \
  --master yarn-client \
  --files fairscheduler.xml \
  --conf spark.scheduler.allocation.file=fairscheduler.xml \
  --conf spark.dynamicAllocation.maxExecutors=$MAX_EXECUTORS \
  --conf spark.io.compression.codec=$CODEC \
  spark-EOS-logs-parser-0.1-SNAPSHOT-jar-with-dependencies.jar \
    /project/monitoring/archive/eos/logs/reports/$EOS_PROJECT \
    /project/awg/eos/processed/parquet-monit/$EOS_PROJECT \
    $ENTRIES_PER_FILE \
    $MAX_THREADS_NUMBER
```

## Run with Docker

Requirements:

* Kerberos ticket
* Hadoop configurations (e.g. to connect to *analytix* Hadoop cluster)
* Docker

Check available versions here: https://gitlab.cern.ch/awg/spark-HTCondor-logs-parser/container_registry

Pull docker image:

```bash
docker pull gitlab-registry.cern.ch/awg/spark-htcondor-logs-parser:v0.1
```

Run with:

```bash
docker run -it --net=host -e KRB5CCNAME=$KRB5CCNAME -e AFS_USER=$USER \
    -v /tmp:/tmp -v /afs:/afs \
    -v /etc/hadoop/conf/:/etc/hadoop/conf \
    gitlab-registry.cern.ch/awg/spark-eos-logs-parser:v0.1 \
    spark-submit \
	  --master yarn-client \
	  --files fairscheduler.xml \
	  --conf spark.scheduler.allocation.file=fairscheduler.xml \
	  --conf spark.dynamicAllocation.maxExecutors=05 \
	  --conf spark.io.compression.codec=snappy \
	  spark-EOS-logs-parser-0.1-SNAPSHOT-jar-with-dependencies.jar \
	    /project/monitoring/archive/eos/logs/reports/cms \
	    /project/awg/eos/processed/parquet-monit/cms \
	    10000000 \
	    50
```
