package ch.cern.awg

import scala.Array.canBuildFrom
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.Promise
import scala.concurrent.TimeoutException
import scala.concurrent.blocking
import scala.concurrent.duration._
import scala.reflect.runtime.universe
import scala.util.Failure
import scala.util.Success
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.sql.Column
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.StructType
import scala.concurrent.ExecutionContext
import java.util.concurrent.Executors
import com.google.common.util.concurrent.ThreadFactoryBuilder
import org.apache.spark.storage.StorageLevel
import org.apache.spark.sql.SaveMode

object EOSHiveConversionDriver {

  def parseOptionString(optionString: Option[Option[String]]): Option[String] = {
    if (optionString.isDefined)
      if (optionString.get.isDefined)
        return Some(optionString.get.get)
    return None
  }

  def parseOptionInt(optionString: Option[Option[String]]): Option[Int] = {
    if (optionString.isDefined)
      if (optionString.get.isDefined)
        try {
          return Some(optionString.get.get.toInt)
        } catch {
          case e: java.lang.NumberFormatException => return None
        }
    return None
  }

  def parseOptionLong(optionString: Option[Option[String]]): Option[Long] = {
    if (optionString.isDefined)
      if (optionString.get.isDefined)
        try {
          return Some(optionString.get.get.toLong)
        } catch {
          case e: java.lang.NumberFormatException => return None
        }
    return None
  }

  def parseOptionDouble(optionString: Option[Option[String]]): Option[Double] = {
    if (optionString.isDefined)
      if (optionString.get.isDefined)
        try {
          return Some(optionString.get.get.toDouble)
        } catch {
          case e: java.lang.NumberFormatException => return None
        }
    return None
  }

  def parseOptionBoolean(optionString: Option[Option[String]]): Option[Boolean] = {
    if (optionString.isDefined)
      if (optionString.get.isDefined)
        try {
          return Some(optionString.get.get.toBoolean)
        } catch {
          case e: java.lang.NumberFormatException => return None
        }
    return None
  }

  private[this] lazy val _sqlContext = {
    val conf = new SparkConf().set("spark.scheduler.mode", "FAIR")
    val sc = new SparkContext(conf)
    val sqlc = new HiveContext(sc)
    (sc, sqlc)
  }

  private[this] var numberOfThreads: Option[Int] = None

  private[this] lazy val sparkPool = {
    val threadFactory =
      new ThreadFactoryBuilder()
        .setDaemon(true)
        .setNameFormat("spark-pool-%s")
        .build()
    if (numberOfThreads.isDefined)
      ExecutionContext.fromExecutor(Executors.newFixedThreadPool(numberOfThreads.get, threadFactory))
    else
      ExecutionContext.fromExecutor(Executors.newCachedThreadPool(threadFactory))
  }

  def stopSpark() {
    val future = Future(_sqlContext._1.stop)(sparkPool)
    Await.result(future, 60.seconds)
  }

  def JSON2Parquet(date: String, inputDayPath: String, outputDayPath: String, flumeSchema: StructType, tableName: String, entriesPerFile: Long, logger: Logger): Future[String] = {

    Future {
      logger.info(s"=> Processing: input=$inputDayPath, output=$outputDayPath, tableName=$tableName")

      val (sc, hiveCtx) = _sqlContext

      val sqlc = hiveCtx.newSession()
      import sqlc.implicits._

      val result = sqlc.read
        .schema(flumeSchema)
        .json(inputDayPath)
        .select("data")
        .where(($"data").isNotNull)
        .distinct()
        .rdd
        .map {
          case Row(data: String) => EOSHiveParser.parse(data.substring(1, data.length - 1))
        }.filter(m => m.isDefined)
        .map(m => m.get)
        .map(m => {
          Row(
            parseOptionString(m.get("path")).getOrElse(null),
            parseOptionInt(m.get("ruid")).getOrElse(null),
            parseOptionInt(m.get("rgid")).getOrElse(null),
            parseOptionString(m.get("td")).getOrElse(null),
            parseOptionString(m.get("host")).getOrElse(null),
            parseOptionInt(m.get("fid")).getOrElse(null),
            parseOptionInt(m.get("fsid")).getOrElse(null),
            parseOptionLong(m.get("ots")).getOrElse(null),
            parseOptionLong(m.get("otms")).getOrElse(null),
            parseOptionLong(m.get("cts")).getOrElse(null),
            parseOptionLong(m.get("ctms")).getOrElse(null),
            parseOptionDouble(m.get("rb")).getOrElse(null),
            parseOptionDouble(m.get("wb")).getOrElse(null),
            parseOptionLong(m.get("sfwdb")).getOrElse(null),
            parseOptionLong(m.get("sbwdb")).getOrElse(null),
            parseOptionLong(m.get("sxlfwdb")).getOrElse(null),
            parseOptionLong(m.get("sxlbwdb")).getOrElse(null),
            parseOptionLong(m.get("nrc")).getOrElse(null),
            parseOptionLong(m.get("nwc")).getOrElse(null),
            parseOptionLong(m.get("nfwds")).getOrElse(null),
            parseOptionLong(m.get("nbwds")).getOrElse(null),
            parseOptionLong(m.get("nxlfwds")).getOrElse(null),
            parseOptionLong(m.get("nxlbwds")).getOrElse(null),
            parseOptionDouble(m.get("rt")).getOrElse(null),
            parseOptionDouble(m.get("wt")).getOrElse(null),
            parseOptionLong(m.get("osize")).getOrElse(null),
            parseOptionLong(m.get("csize")).getOrElse(null),
            parseOptionString(m.get("sec.name")).getOrElse(null),
            parseOptionString(m.get("sec.host")).getOrElse(null),
            parseOptionString(m.get("sec.vorg")).getOrElse(null),
            parseOptionString(m.get("sec.grps")).getOrElse(null),
            parseOptionString(m.get("sec.role")).getOrElse(null),
            parseOptionString(m.get("sec.app")).getOrElse(null))
        })

      val resultDF = sqlc.createDataFrame(
        result,
        new StructType()
          .add("path", "string")
          .add("ruid", "int")
          .add("rgid", "int")
          .add("td", "string")
          .add("host", "string")
          .add("fid", "int")
          .add("fsid", "int")
          .add("ots", "long")
          .add("otms", "long")
          .add("cts", "long")
          .add("ctms", "long")
          .add("rb", "double")
          .add("wb", "double")
          .add("sfwdb", "long")
          .add("sbwdb", "long")
          .add("sxlfwdb", "long")
          .add("sxlbwdb", "long")
          .add("nrc", "long")
          .add("nwc", "long")
          .add("nfwds", "long")
          .add("nbwds", "long")
          .add("nxlfwds", "long")
          .add("nxlbwds", "long")
          .add("rt", "double")
          .add("wt", "double")
          .add("osize", "long")
          .add("csize", "long")
          .add("secname", "string")
          .add("sechost", "string")
          .add("secvorg", "string")
          .add("secgrps", "string")
          .add("secrole", "string")
          .add("secapp", "string"))

      resultDF.persist(StorageLevel.MEMORY_AND_DISK)
      val count = resultDF.count()
      logger.info(s"=> date: $date, path $inputDayPath, total: $count")
      sc.setLocalProperty("spark.scheduler.pool", "speed")
      resultDF
        .repartition(if (count / entriesPerFile != 0) Math.ceil(count / entriesPerFile).toInt else 1)
        .write
        .mode(SaveMode.Overwrite)
        .parquet(outputDayPath)
      resultDF.unpersist()

      s"=> input=$inputDayPath, result=$outputDayPath, count=$count"
    }(sparkPool)
  }

  def main(args: Array[String]) {

    var logger = Logger.getLogger(EOSHiveConversionDriver.this.getClass())

    if (args.length < 3) {
      logger.error("=> wrong number of parameters")
      System.err.println("Usage: EOSHiveConversionDriver <0:raw flume input path> <1:month output path> <2: entriesPerFile> <3: numThread (Optional)>")
      System.exit(1)
    }

    if (args.length == 4)
      this.numberOfThreads = Some(args(3).toInt)

    val future = Future(_sqlContext)(sparkPool)
    Await.result(future, 80.second)
    val (sc, sqlc) = future.value.get.get

    val eosFlumePath = args(0)
    val outputPath = args(1)
    val entriesPerFile = args(2).toLong

    val fs = FileSystem.get(sc.hadoopConfiguration)

    logger.info(s"=> eosFlumePath $eosFlumePath")
    logger.info(s"=> outputPath $outputPath")
    logger.info(s"=> entriesPerFile $entriesPerFile")

    val yearsList = fs.listStatus(new Path(eosFlumePath))
    val monthsList = fs.listStatus(yearsList.map(_.getPath))
    val daysList = fs.listStatus(monthsList.map(_.getPath))

    val daysToProcess = daysList.map(fstatus => (fstatus.getPath.toString.split("/").toList.takeRight(3), fstatus))
      .map { case (date, fstatus) => (date(0), date(1), date(2), fstatus) }
      .filter { case (year, month, day, fstatus) => !day.contains(".tmp") && year.matches("20[0-9][0-9]") }
      .map { case (year, month, day, fstatus) => (year, month, day, fstatus, s"$outputPath/year=${year.toInt}/month=${month.toInt}/day=${day.toInt}") }
      .filter {
        case (year, month, day, fstatus, outputDayPath) =>
          {
            val parquetPath = new Path(outputDayPath)
            if (fs.exists(parquetPath))
              if (fs.getFileStatus(parquetPath).getModificationTime < fstatus.getModificationTime)
                true
              else {
                if (!fs.exists(new Path(outputDayPath + "/_SUCCESS")))
                  true
                else
                  false
              }
            else
              true
          }
      }

    var futureResults = new Array[Future[String]](daysToProcess.length)

    val futures = for (i <- 0 to daysToProcess.length - 1) yield {
      val date = s"${daysToProcess(i)._1}${daysToProcess(i)._2}${daysToProcess(i)._3}"
      val inputDayPath = daysToProcess(i)._4.getPath.toString
      val outputDayPath = daysToProcess(i)._5
      val tableName = s"eosTmpTable$date"
      val flumeSchema = new StructType().add("data", "string")
      JSON2Parquet(date, inputDayPath, outputDayPath, flumeSchema, tableName, entriesPerFile, logger)
    }

    logger.info(s"=> Processing ${daysToProcess.length} days.")

    val f = Future.sequence(futures.toList)

    Await.result(f, Duration.Inf)

    logger.info(s"=> Parallel execution completed.")

    f onComplete {
      case Success(messages) => for (message <- messages) logger.info(message)
      case Failure(t) => logger.error("An error has occured: " + t.getMessage + " " + t.toString())
    }

    stopSpark()

  }
}