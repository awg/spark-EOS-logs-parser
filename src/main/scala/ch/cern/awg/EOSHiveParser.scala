package ch.cern.awg

@SerialVersionUID(1L)
object EOSHiveParser {

  @volatile def parse(line: String): Option[Map[String, Option[String]]] = {
    
    if (line == null){
      return None
    }
    
    try {
      var pos = 0
      var end = -1
      var equalPosition = -1
      var resultMap: Map[String, Option[String]] = Map()
      var lastKeyAdded: Option[String] = None
      var counter = 0

      end = line.indexOf("&", pos)
      while (end >= 0) {
        val keyValue = line.substring(pos, end);
        equalPosition = keyValue.indexOf("=");
        if (equalPosition == -1) {
          if (lastKeyAdded.isDefined && !keyValue.equals("")) {
            val update = resultMap(lastKeyAdded.get).get + "&" + keyValue
            resultMap -= lastKeyAdded.get
            resultMap = resultMap + (lastKeyAdded.get -> Some(update))
          }
        } else {
          val key = keyValue.substring(0, equalPosition)
          val value = keyValue.substring(equalPosition + 1)
          if (value.equals("")) {
            resultMap = resultMap + (key -> None)
          } else {
            resultMap = resultMap + (key -> Some(value))
          }
          counter += 1
          lastKeyAdded = Some(key)
        }
        pos = end + 1
        end = line.indexOf("&", pos)
      }

      val keyValue = line.substring(pos)

      if (!keyValue.equals("")) {
        equalPosition = keyValue.indexOf("=")
        if (equalPosition == -1) {
          if (lastKeyAdded.isDefined) {
            val update = resultMap(lastKeyAdded.get).get + "&" + keyValue
            resultMap -= lastKeyAdded.get
            resultMap = resultMap + (lastKeyAdded.get -> Some(update))
          }
        } else {
          val key = keyValue.substring(0, equalPosition)
          val value = keyValue.substring(equalPosition + 1)
          if (value.equals("")) {
            resultMap = resultMap + (key -> None)
          } else {
            resultMap = resultMap + (key -> Some(value))
          }
          counter += 1
        }
      }

      return Some(resultMap)

    } catch {
      case e: IndexOutOfBoundsException => return None
      case e: java.util.NoSuchElementException => return None
    }
  }

}