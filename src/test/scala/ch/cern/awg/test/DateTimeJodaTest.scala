package ch.cern.awg.test

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.Matchers._
import org.joda.time.Days

class DateTimeJodaTest extends FlatSpec with Matchers {

  val sparkConf = new SparkConf()
  sparkConf.setMaster("local[*]").setAppName("test")
  val sparkContext = new SparkContext(sparkConf)

  "Joda libraries" should "parse date without day" in {
    val monthParsed = DateTime.parse("201601 UTC", DateTimeFormat.forPattern("yyyyMM z"))
  }

  an[java.lang.IllegalArgumentException] should be thrownBy {
    DateTime.parse("1 UTC", DateTimeFormat.forPattern("yyyyMM z"))
  }

  an[java.lang.IllegalArgumentException] should be thrownBy {
    DateTime.parse("UTC", DateTimeFormat.forPattern("yyyyMM z"))
  }

  val month = DateTime.parse("201601 UTC", DateTimeFormat.forPattern("yyyyMM z"))

  it should "return the first of the month" in {
    month.dayOfMonth().get() should be(1)
  }

  it should "return January as month" in {
    month.monthOfYear().get() should be(1)
    month.monthOfYear().getAsShortText() should be("Jan")
  }

  it should "print the YYYY-MM-DD date" in {
    month.toYearMonthDay().toString() should be("2016-01-01")
  }

  val nextMonth = DateTime.parse("201602 UTC", DateTimeFormat.forPattern("yyyyMM z"))
  
  it should "create plain YYYYMMDD date" in {
    month.toString("yyyyMMdd") should be("20160101")
    nextMonth.toString("yyyyMMdd") should be("20160201")
    month.plusMonths(2).toString("yyyyMMdd") should be("20160301")
  }

  "Next month method" should "return the first of Feb" in {
    month.plusMonths(1) should equal(nextMonth)
    month.plusMonths(1).dayOfMonth().get() should be(1)
    nextMonth.plusMonths(1) should equal(month.plusMonths(2))
  }

  "Get millis" should "return second since epoch *1000" in {
    month.getMillis() should be(1451606400L * 1000)
  }

  "Days" should "be calculated with difference with dates" in {
    Days.daysBetween(month, nextMonth).getDays() should be(31)
  }

  // not really a test
  "iterating days" should "be done using for statement" in {
    val daysCount = Days.daysBetween(month, nextMonth).getDays()
    (1 to daysCount).map(month.plusDays(_)).foreach(println)
  }
  
  "With not the first of the month" should "give the first of the next month when calculating which are the days" in {
    val middleJanDate = DateTime.parse("20160101 UTC", DateTimeFormat.forPattern("yyyyMMdd z"))
    middleJanDate.toString("yyyyMMdd") should be("20160101")
    val firstDayNextMonth = middleJanDate.plusMonths(1).minusDays(middleJanDate.plusMonths(1).getDayOfMonth()-1)
    firstDayNextMonth.toString("yyyyMMdd") should be("20160201")
  }
  
  "DateTime parse with seconds" should "be parsed in long" in {
    val startTimestamp = "2016-01-01 00:00:00" 
    val startTimestampParsed = DateTime.parse(startTimestamp + " UTC", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss z"))
    startTimestampParsed.getMillis() should be(1451606400L * 1000)
  }

  sparkContext.stop()

}